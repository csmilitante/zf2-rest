<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
    'db' => [
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=exam3;host=localhost',
        'driver_options' => [
            1002 => 'SET NAMES \'UTF8\''
    ],
    ],
    'jwt' => [
        'key' => 's5BKIznm9+Upqr6LNpZM2yuEg6400IgErkrlt58W29kB8BhlkUkiCHpleNMP4342aNMHyG/FFP/ztF62uN2G9w==', //base64_encode(openssl_random_pseudo_bytes(64))
        'alg' => 'HS256',
        'iss' => 'http://api.exam.com', // OPTIONAL
        'exp_offset' => 3600, // seconds
        'nbf_offset' => 0, // seconds
    ]
];
