<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

// Products
use Carts\Controller\Rest\CartsRestController;
use Carts\Controller\Rest\CartItemsRestController;
use Products\Controller\Rest\ProductsRestController;
use JobOrders\Controller\Rest\JobOrdersRestController;
use Customers\Controller\Rest\CustomersRestController;
use Shipping\Controller\Rest\ShippingRestController;

use Application\Controller\IndexController;
use Application\Factory\IndexControllerFactory;
use Zend\Mvc\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'api' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api',
                    'defaults' => [
                        'controller' => ProductsRestController::class
                    ]
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'products' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/products[/:id]',
                            'defaults' => [
                                'controller' => ProductsRestController::class
                            ]
                        ],
                    ],
                    'carts' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/carts[/:id]',
                            'defaults' => [
                                'controller' => CartsRestController::class
                            ]
                        ],
                    ],
                    'carts_items' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/cart-items[/:id]',
                            'defaults' => [
                                'controller' => CartItemsRestController::class
                            ]
                        ],
                    ],
                    'shipping' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/shipping[/:id]',
                            'defaults' => [
                                'controller' => ShippingRestController::class
                            ]
                        ],
                    ],
                    'customers' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/customers[/:id]',
                            'defaults' => [
                                'controller' => CustomersRestController::class
                            ]
                        ],
                    ],
                    'job_orders' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/orders[/:id]',
                            'defaults' => [
                                'controller' => JobOrdersRestController::class
                            ]
                        ],
                    ]
                ]
            ],
            'home' => [
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/application',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults' => [
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ],
        'aliases' => [
            'translator' => 'MvcTranslator',
        ],
        'factories' => [
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ]
    ],
    'input_filters' => [
        'abstract_factories' => [
            'Zend\InputFilter\InputFilterAbstractServiceFactory'
        ]
    ],
    'translator' => [
        'locale' => 'en_US',
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ],
        ],
    ],
    'controllers' => [
        'aliases' => [
            'Application\Controller\Index' => IndexController::class,
        ],
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ]
    ],
    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy'
        ],
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    // Placeholder for console routes
    'console' => [
        'router' => [
            'routes' => [
            ],
        ],
    ],
];
