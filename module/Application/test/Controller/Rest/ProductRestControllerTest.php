<?php

namespace ApplicationTest\Rest\Controller;

use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Products\Controller\Rest\ProductRestController;

class ProductRestControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.

        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            $configOverrides
        ));

        parent::setUp();

        $services = $this->getApplicationServiceLocator();
        $config = $services->get('config');
        unset($config['db']);
        $services->setAllowOverride(true);
        $services->setService('config', $config);
        $services->setAllowOverride(false);
    }

    public function statusCodes()
    {
        return [
            'POST_NO_ID' => ['/api/products', 'POST', 201],
            'GET_WITH_ID' => ['/api/products/12', 'GET', 200],
            'GET_NO_ID' => ['/api/products', 'GET', 200],
            'UPDATE_WITH_ID' => ['/api/products/12', 'PUT', 200],
            'UPDATE_NO_ID' => ['/api/products', 'PUT', 405],
            'DELETE_NO_ID' => ['/api/products', 'DELETE', 405],
        ];
    }

    /**
     * @dataProvider statusCodes
     */
    public function testStatusCodeReturn($url, $httpMethod, $statusCode)
    {
        $this->dispatch($url, $httpMethod);
        $this->assertResponseStatusCode($statusCode);
    }

    /*public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/api/products/1', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(ProductRestController::class); // as specified in router's controller name alias
        $this->assertControllerClass('ProductRestController');
        $this->assertMatchedRouteName('api');
    }*/

    /*public function testIndexActionViewModelTemplateRenderedWithinLayout()
    {
        $this->dispatch('/', 'GET');
        $this->assertQuery('.container .jumbotron');
    }*/

    /*public function testInvalidRouteDoesNotCrash()
    {
        $this->dispatch('/invalid/route', 'GET');
        $this->assertResponseStatusCode(404);
    }*/
}
