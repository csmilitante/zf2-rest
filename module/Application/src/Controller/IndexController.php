<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use \Firebase\JWT\JWT;

class IndexController extends AbstractActionController
{
    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function indexAction()
    {
        $jwtConfig = $this->config['jwt'];
        $key = base64_decode($jwtConfig['key']);

        $tokenId = uniqid();
        $issuedAt = time();
        $notBefore = $issuedAt + 10;
        $expire = $notBefore + 60;

        $data = [
            'username' => 'stak',
            'id' => 1
        ];

        $jwt = [
            'iat' => $issuedAt,
            'jti' => $tokenId,
            'nbf' => $notBefore,
            'exp' => $expire,
            'iss' => $jwtConfig['iss'], // should be in the config
            'data' => $data
        ];

        $jwt = JWT::encode($jwt, $key);


        return new JsonModel(
            [ 'jwt' => $jwt ]
        );
    }

    public function getDecodedJwtAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost()->toArray();

        $jwtConfig = $this->config['jwt'];
        $key = base64_decode($jwtConfig['key']);

        try {
            $decoded = JWT::decode($post['jwt'], $key, [$jwtConfig['alg']]);

            return new JsonModel(
                [
                    'decoded' => (array) $decoded
                ]
            );
        } catch (\Exception $e) {
            return new JsonModel(
                [
                    'error' => $e->getMessage()
                ]
            );
        }
    }
}
