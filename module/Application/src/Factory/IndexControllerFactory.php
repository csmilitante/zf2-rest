<?php

namespace Application\Factory;

use Application\Controller\IndexController;
use Psr\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class IndexControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        $sl = $container->getServiceLocator();
        $config = $sl->get('config');

        return new IndexController($config);
    }

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $container
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container, IndexController::class);
    }
}
