<?php

namespace Customers\Model;

use Zend\Db\TableGateway\TableGateway;

class CustomersTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function create(Customers $customers)
    {
        $data = [
            'customer_id' => $customers->customer_id,
            'email' => $customers->email,
            'password' => $customers->password,
            'company_name' => $customers->company_name,
            'first_name' => $customers->first_name,
            'last_name' => $customers->last_name,
            'phone' => $customers->phone,
        ];

        try {
            $this->tableGateway->insert($data);
            return $this->tableGateway->getLastInsertValue();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getById($customerId)
    {
        $id = (int) $customerId;
        $rowset = $this->tableGateway->select(['customer_id' => $id]);

        $row = $rowset->current();

        if (!$row) {
            throw new \Exception('Could not find customer record', 500);
        }

        return $row;
    }

    public function getByEmail($email)
    {
        $rowset = $this->tableGateway->select(['email' => $email]);

        $row = $rowset->current();

        if (!$row) {
            return false;
        }

        return $row;
    }

    public function getByEmailAndPassword($email, $password)
    {
        $rowset = $this->tableGateway->select(['email' => $email, 'password' => $password]);

        $row = $rowset->current();

        if (empty($row)) {
            throw new \Exception('Could not find customer record', 401);
        }

        return $row;
    }
}
