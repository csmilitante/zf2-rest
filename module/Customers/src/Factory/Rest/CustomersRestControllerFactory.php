<?php

namespace Customers\Factory\Rest;

use Customers\Model\CustomersTable;
use Customers\Controller\Rest\CustomersRestController;
use Customers\InputFilter\CreateCustomerFilter;
use Customers\InputFilter\LoginFilter;
use Carts\Model\CartsTable;
use Interop\Container\ContainerInterface;

class CustomersRestControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $sl = $container->getServiceLocator();
        $customersTable = $sl->get(CustomersTable::class);
        $cartsTable = $sl->get(CartsTable::class);
        $config = $sl->get('config');
        $createCustomerFilter = new CreateCustomerFilter();
        $loginFilter = new LoginFilter();

        $serverFactory = $sl->get('ZF\OAuth2\Service\OAuth2Server');
        $server = call_user_func($serverFactory, 'oauth');

        return new CustomersRestController(
            $loginFilter,
            $createCustomerFilter,
            $customersTable,
            $cartsTable,
            $server,
            $config
        );
    }
}
