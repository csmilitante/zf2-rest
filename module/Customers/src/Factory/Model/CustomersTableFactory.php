<?php

namespace Customers\Factory\Model;

use Customers\Model\Customers;
use Customers\Model\CustomersTable;
use Interop\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class CustomersTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Customers());

        $customersTableGateway = new TableGateway('customers', $dbAdapter, null, $resultSetPrototype);

        return new CustomersTable($customersTableGateway);
    }
}
