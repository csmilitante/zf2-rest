<?php

namespace Customers\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;

class LoginFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(
            [
                'name' => 'email',
                'required' => true,
                'filters' => [
                    [
                        'name' => 'Zend\Filter\StringTrim',
                        'options' => [],
                    ],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\EmailAddress::class,
                    ],
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'password',
                'required' => true,
                'filters' => [
                    [
                        'name' => 'Zend\Filter\StringTrim',
                        'options' => [],
                    ],
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );
    }
}
