<?php

namespace Customers\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;

class CreateCustomerFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(
            [
                'name' => 'email',
                'required' => true,
                'filters' => [
                    [
                        'name' => 'Zend\Filter\StringTrim',
                        'options' => [],
                    ],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\EmailAddress::class,
                    ],
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'password',
                'required' => true,
                'filters' => [
                    [
                        'name' => 'Zend\Filter\StringTrim',
                        'options' => [],
                    ],
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'confirm_password',
                'required' => true,
                'filters' => [
                    [
                        'name' => 'Zend\Filter\StringTrim',
                        'options' => [],
                    ],
                ],
                'validators' => [
                    [
                        'name' => \Zend\Validator\Identical::class,
                        'options' => [
                            'token' => 'password'
                        ]
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'company_name',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'first_name',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'last_name',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'type',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ],
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );
    }
}
