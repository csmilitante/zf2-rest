<?php

namespace Customers\Controller\Rest;

use Application\CustomExceptions\FormFilterException;
use Customers\InputFilter\CreateCustomerFilter;
use Customers\InputFilter\LoginFilter;
use Carts\Model\CartsTable;
use Customers\Model\Customers;
use Customers\Model\CustomersTable;
use Zend\View\Model\JsonModel;

class CustomersRestController extends BaseRestController
{
    /**
     * @var CustomersTable
     */
    private $customersTable;
    /**
     * @var CartsTable
     */
    private $cartsTable;
    /**
     * @var CreateCustomerFilter
     */
    private $createCustomerFilter;
    /**
     * @var LoginFilter
     */
    private $loginFilter;

    public function __construct(
        LoginFilter $loginFilter,
        CreateCustomerFilter $createCustomerFilter,
        CustomersTable $customersTable,
        CartsTable $cartsTable,
        $server,
        $config
    ) {
        parent::__construct($config, $server);
        $this->customersTable = $customersTable;
        $this->cartsTable = $cartsTable;
        $this->createCustomerFilter = $createCustomerFilter;
        $this->loginFilter = $loginFilter;
    }

    public function get($id)
    {
        $response = $this->getResponse();

        try {
            $response->setStatusCode(200);
            $customers = $this->customersTable->getById($id);
            return new JsonModel(
                [
                    'customers' => $customers
                ]
            );
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function create($postData)
    {
        try {
            $response = $this->getResponse();

            // Check type
            if (!empty($postData['type']) && $postData['type'] === 'registration') {
                $this->register($postData);
                $response->setStatusCode(201);
            }

            // Login
            $returnData = $this->login($postData);
            $response->setStatusCode(200);

            return new JsonModel($returnData);
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    private function login($postData)
    {
        try {
            $this->loginFilter->setData($postData);

            if (!$this->loginFilter->isValid()) {
                throw new FormFilterException(
                    'Form data is invalid. Please check try again.',
                    406
                );
            }

            $data = [];
            $customer = $this->customersTable->getByEmailAndPassword($postData['email'], $postData['password']);

            // Remove password since this should not be made public
            unset($customer->password);

            // Create auth_token
            $authToken = $this->generateJwtToken($customer);
            $data['access_token'] = $authToken;
            $data['customer']['first_name'] = $customer->first_name;

            if (!empty($postData['jwt'])) {
                $decodedJwt = $this->decodeJwtToken($postData['jwt']);

                // Update current JWT with customer ID
                $decodedJwt['customers']['customer_id'] = $customer->customer_id;
                $decodedJwt['customers']['first_name'] = $customer->first_name;
                $decodedJwt['customers']['last_name'] = $customer->last_name;

                // Update cart
                $cartsEntity = $this->cartsTable->getById($decodedJwt['carts']['cart_id']);

                $customerArr = $customer->toArray();
                foreach ($customerArr as $key => $value) {
                    $cartsEntity->{$key} = $value;
                }

                $this->cartsTable->upsert($cartsEntity);

                // re encode jwt
                $data['jwt'] = $this->generateJwtToken($decodedJwt);
            }

            return $data;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function register($postData)
    {
        try {
            // Validate postData
            $this->createCustomerFilter->setData($postData);

            if (!$this->createCustomerFilter->isValid()) {
                throw new \Exception(
                    json_encode($this->createCustomerFilter->getMessages()),
                    406
                );
            }

            // check if email id unique else return error
            $emailExists = $this->customersTable->getByEmail($postData['email']);

            if ($emailExists) {
                throw new \Exception('Email already exists', 500);
            }

            $customers = new Customers();
            $customers->exchangeArray($postData);

            return $this->customersTable->create($customers);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
