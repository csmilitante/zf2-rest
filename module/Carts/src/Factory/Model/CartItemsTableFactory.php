<?php

namespace Carts\Factory\Model;

use Carts\Model\CartItems;
use Carts\Model\CartItemsTable;
use Interop\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class CartItemsTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new CartItems());

        $cartItemsTableGateway = new TableGateway('cart_items', $dbAdapter, null, $resultSetPrototype);

        return new CartItemsTable($cartItemsTableGateway);
    }
}
