<?php

namespace Carts\Factory\Model;

use Carts\Model\Carts;
use Carts\Model\CartsTable;
use Interop\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class CartsTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Carts());

        $cartsTableGateway = new TableGateway('carts', $dbAdapter, null, $resultSetPrototype);

        return new CartsTable($cartsTableGateway);
    }
}
