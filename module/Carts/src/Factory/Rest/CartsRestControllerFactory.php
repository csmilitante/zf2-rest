<?php

namespace Carts\Factory\Rest;

use Carts\Controller\Rest\CartsRestController;
use Carts\Model\CartItemsTable;
use Carts\Model\CartsTable;
use Products\Model\ProductsTable;
use Carts\InputFilter\CreateCartFilter;
use Interop\Container\ContainerInterface;

class CartsRestControllerFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $sl = $container->getServiceLocator();
        $cartsTable = $sl->get(CartsTable::class);
        $productsTable = $sl->get(ProductsTable::class);
        $cartItemsTable = $sl->get(CartItemsTable::class);
        $config = $sl->get('config');
        $createCartFilter = new CreateCartFilter();

        $serverFactory = $sl->get('ZF\OAuth2\Service\OAuth2Server');
        $server = call_user_func($serverFactory, 'oauth');

        return new CartsRestController(
            $createCartFilter,
            $cartsTable,
            $productsTable,
            $cartItemsTable,
            $server,
            $config
        );
    }
}
