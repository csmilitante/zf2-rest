<?php

namespace Carts\Factory\Rest;

use Carts\Controller\Rest\CartItemsRestController;
use Carts\Model\CartItemsTable;
use Carts\Model\CartsTable;
use Interop\Container\ContainerInterface;

class CartItemsRestControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $sl = $container->getServiceLocator();
        $cartItemsTable = $sl->get(CartItemsTable::class);
        $cartsTable = $sl->get(CartsTable::class);
        $config = $sl->get('config');

        $serverFactory = $sl->get('ZF\OAuth2\Service\OAuth2Server');
        $server = call_user_func($serverFactory, 'oauth');

        return new CartItemsRestController(
            $cartItemsTable,
            $cartsTable,
            $config,
            $server
        );
    }
}
