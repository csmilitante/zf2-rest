<?php

namespace Carts\Controller\Rest;

use Carts\InputFilter\CreateCartFilter;
use Carts\Model\CartItems;
use Carts\Model\CartItemsTable;
use Carts\Model\Carts;
use Carts\Model\CartsTable;
use Products\Model\Product;
use Products\Model\ProductsTable;
use Zend\View\Model\JsonModel;

class CartsRestController extends BaseRestController
{
    /** @var ProductsTable */
    protected $productsTable;
    /** @var CartsTable */
    protected $cartsTable;
    /** @var CartItemsTable */
    protected $cartItemsTable;
    private $createCartFilter;

    public function __construct(
        CreateCartFilter $createCartFilter,
        CartsTable $cartsTable,
        ProductsTable $productsTable,
        CartItemsTable $cartItemsTable,
        $server,
        $config
    ) {
        parent::__construct($config, $server);
        $this->cartsTable = $cartsTable;
        $this->productsTable = $productsTable;
        $this->cartItemsTable = $cartItemsTable;
        $this->createCartFilter = $createCartFilter;
    }

    public function create($postData)
    {

        try {
            $response = $this->getResponse();

            // Validate post data here
            $this->createCartFilter->setData($postData);

            if (!$this->createCartFilter->isValid()) {
                throw new \Exception(
                    json_encode($this->createCartFilter->getMessages()),
                    406
                );
            }

            $authHeader = $this->getAuthHeader();

            if ($authHeader !== false) {
                $decodedAccessToken = $this->decodeJwtToken($this->getAccessToken());
                $postData['customer_id'] = $decodedAccessToken['customer_id'];
                $postData['email'] = $decodedAccessToken['email'];
                $postData['company_name'] = $decodedAccessToken['company_name'];
                $postData['first_name'] = $decodedAccessToken['first_name'];
                $postData['last_name'] = $decodedAccessToken['last_name'];
                $postData['phone'] = $decodedAccessToken['phone'];
            }

            // get product details based on passed product ID
            $product = $this->productsTable->getById($postData['pid']);

            // compute sub_total
            $postData['sub_total'] = (int)$postData['quantity'] * $product->price;

            // compute weight
            $postData['total_weight'] = (int)$postData['quantity'] * $product->weight;

            // compute total_amount
            $postData['total_amount'] = $postData['sub_total'];

            // do only when jwt is valid is not null
            if (!empty($postData['jwt'])) {
                // Decode jwt
                $decoded = $this->decodeJwtToken($postData['jwt']);

                // recompute sub_total and weight
                $postData['cart_id'] = $decoded['carts']['cart_id'];
                $existingCart = $this->cartsTable->getById($postData['cart_id']);

                // Add and recompute sub_total and total_weight from post data to the existing
                $postData['sub_total'] = $existingCart->sub_total + $postData['sub_total'];
                $postData['total_weight'] = $existingCart->total_weight + $postData['total_weight'];
                $postData['total_amount'] = $existingCart->total_amount + $postData['total_amount'];
            }

            $cartsEntity = new Carts();
            $cartsEntity->exchangeArray($postData);

            $cartId = $this->cartsTable->upsert($cartsEntity);
            $response->setStatusCode(201);

            // Add items to cart
            $this->addItemsToCart($product, $postData['quantity'], $cartId);

            $payload = [
                'carts' => [
                    'cart_id' => $cartId,
                ]
            ];

            $jwt = $this->generateJwtToken($payload);

            // send back data
            return new JsonModel(['jwt' => $jwt]);
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function get($jwt)
    {
        try {
            $response = $this->getResponse();

            // decode jwt token
            $decoded = $this->decodeJwtToken($jwt);

            // Get cart details
            $cart = $this->cartsTable->getById($decoded['carts']['cart_id']);

            // Get cart items
            $cartItems = $this->cartItemsTable->getByCartId($decoded['carts']['cart_id']);

            // Get product details of cart items
            foreach ($cartItems as $index => $item) {
                $product = $this->productsTable->getById($item->product_id);
                $item->product_details = $product;
                $cartItems[$index] = $item;
            }

            $response->setStatusCode(200);

            return new JsonModel(
                [
                    'carts' => [
                        'cart' => $cart,
                        'cart_items' => $cartItems
                    ],
                    '_links' => [
                        'to_cart' => '/api/carts'
                    ]
                ]
            );
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function getList()
    {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-type', 'application/hal+json');
        $response->setStatusCode(200);

        $cartsList = $this->cartsTable->fetchAll()->toArray();

        return new JsonModel(
            [
                'carts' => $cartsList,
                '_links' => [
                    'view' => '/api/carts/[cart_id]'
                ]
            ]
        );
    }

    public function deleteList($data)
    {
        try {
            $response = $this->getResponse();
            // Decode jwt
            $decodedJwt = $this->decodeJwtToken($data['jwt']);

            $return = $this->cartsTable->deleteById($decodedJwt['carts']['cart_id']);

            $response->setStatusCode(200);

            return new JsonModel([
                'response' => $return
            ]);
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    /**
     * @param Product $product
     * @param         $qty
     * @param         $cartId
     *
     * @return int
     * @throws \Exception
     */
    private function addItemsToCart(Product $product, $qty, $cartId)
    {
        try {
            // prepare data
            $data = [
                'cart_id' => $cartId,
                'product_id' => $product->product_id,
                'weight' => $product->weight * (int) $qty,
                'qty' => $qty,
                'unit_price' => $product->price,
                'price' => $product->price * $qty
            ];

            $cartItemsEntity = new CartItems();
            $cartItemsEntity->exchangeArray($data);

            return $this->cartItemsTable->create($cartItemsEntity);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
