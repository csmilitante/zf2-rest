<?php

namespace Carts\Controller\Rest;

use Carts\Model\CartItemsTable;
use Carts\Model\Carts;
use Carts\Model\CartsTable;
use Zend\View\Model\JsonModel;

class CartItemsRestController extends BaseRestController
{
    /**
     * @var CartItemsTable
     */
    private $cartItemsTable;
    /**
     * @var CartsTable
     */
    private $cartsTable;

    public function __construct(
        CartItemsTable $cartItemsTable,
        CartsTable $cartsTable,
        $config,
        $server
    ) {
        parent::__construct($config, $server);
        $this->cartItemsTable = $cartItemsTable;
        $this->cartsTable = $cartsTable;
    }

    public function get($id)
    {
        try {
            $response = $this->getResponse();
            $carts = $this->cartsTable->getById($id);
            $response->setStatusCode(200);

            return new JsonModel(
                [
                    'carts' => $carts,
                    '_links' => [
                        'to_cart' => '/api/carts'
                    ]
                ]
            );
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function getList()
    {
        try {
            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-type', 'application/hal+json');
            $response->setStatusCode(200);

            $cartsList = $this->cartsTable->fetchAll()->toArray();

            return new JsonModel(
                [
                    'carts' => $cartsList,
                    '_links' => [
                        'view' => '/api/carts/[cart_id]'
                    ]
                ]
            );
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function deleteList($payload)
    {
        // Delete by product id and cart id
        try {
            $response = $this->getResponse();

            // Decode jwt
            $decodedJwt = $this->decodeJwtToken($payload['jwt']);

            // Get cart details
            $cart = $this->cartsTable->getById($decodedJwt['carts']['cart_id']);
            // Get cart item details before deleting
            $cartItemRowset = $this->cartItemsTable->getByProductIdAndCartId(
                $payload['product_id'],
                $decodedJwt['carts']['cart_id']
            );

            $cartItem = $cartItemRowset->current();

            // Update cart | prepare data
            $updatedCartData = [
                'cart_id' => $cart->cart_id,
                'total_weight' => $cart->total_weight - $cartItem->weight,
                'total_amount' => $cart->total_amount - $cartItem->price,
                'sub_total' => $cart->sub_total - $cartItem->price
            ];

            $cartsEntity = new Carts();
            $cartsEntity->exchangeArray($updatedCartData);
            $this->cartsTable->upsert($cartsEntity);

            // delete cart item
            $this->cartItemsTable->deleteByCartIdAndProductId($decodedJwt['carts']['cart_id'], $payload['product_id']);

            // return updated jwt
            $jwt = $this->generateJwtToken($decodedJwt);

            $response->setStatusCode(200);

            return new JsonModel([
                'jwt' => $jwt
            ]);
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }
}
