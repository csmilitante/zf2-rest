<?php

namespace Carts\Model;

use Zend\Db\TableGateway\TableGateway;

class CartsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getById($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['cart_id' => $id]);
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception('Cart cannot be found.', 500);
        }

        return $row;
    }

    public function upsert(Carts $carts)
    {
        $data = [
            'customer_id' => $carts->customer_id,
            'order_datetime' => $carts->order_datetime,
            'sub_total' => $carts->sub_total,
            'taxable_amount' => $carts->taxable_amount,
            'discount' => $carts->discount,
            'tax' => $carts->tax,
            'shipping_total' => $carts->shipping_total,
            'total_amount' => $carts->total_amount,
            'total_weight' => $carts->total_weight,
            'company_name' => $carts->company_name,
            'email' => $carts->email,
            'first_name' => $carts->first_name,
            'last_name' => $carts->last_name,
            'phone' => $carts->phone,
            'shipping_mehod' => $carts->shipping_mehod,
            'shipping_name' => $carts->shipping_name,
            'shipping_address1' => $carts->shipping_address1,
            'shipping_address2' => $carts->shipping_address2,
            'shipping_address3' => $carts->shipping_address3,
            'shipping_city' => $carts->shipping_city,
            'shipping_state' => $carts->shipping_state,
            'shipping_country' => $carts->shipping_country,
        ];

        $id = (int) $carts->cart_id;

        if ($id == 0) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getById($id)) {
                $this->tableGateway->update($data, ['cart_id' => $id]);

                // Return cart id
                return $id;
            } else {
                throw new \Exception('Record does not exist', 500);
            }
        }
    }

    public function deleteById($id)
    {
        try {
            return $this->tableGateway->delete(['cart_id' => (int) $id]);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
