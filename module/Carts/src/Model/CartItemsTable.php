<?php

namespace Carts\Model;

use Zend\Db\TableGateway\TableGateway;

class CartItemsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getByProductIdAndCartId($productId, $cartId)
    {
        $rowset = $this->tableGateway->select([
            'product_id' => (int)$productId,
            'cart_id' => (int)$cartId
        ]);

        if (!$rowset) {
            return false;
        }

        return $rowset;
    }

    /**
     * @param $id
     *
     * @return array $row
     * @throws \Exception
     */
    public function getByCartId($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['cart_id' => $id]);

        if ($rowset->count() <= 0) {
            throw new \Exception('Could not find record cart item', 500);
        }

        foreach ($rowset as $cartItem) {
            $row[] = $cartItem;
        }

        return $row;
    }

    public function getById($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['cart_item_id' => $id]);
        $row = $rowset->current();

        if (!$rowset) {
            throw new \Exception('Could not find cart item', 500);
        }

        return $row;
    }

    public function create(CartItems $cartItems)
    {
        $data = [
            'cart_item_id' => $cartItems->cart_item_id,
            'cart_id' => $cartItems->cart_id,
            'product_id' => $cartItems->product_id,
            'weight' => $cartItems->weight,
            'qty' => $cartItems->qty,
            'unit_price' => $cartItems->unit_price,
            'price' => $cartItems->price,
        ];

        $cartId = (int)$cartItems->cart_id;
        $productId = (int)$cartItems->product_id;

        $existingCartItem = $this->getByProductIdAndCartId($productId, $cartId);

        // Update items in the cart if found
        if ($existingCartItem->count() > 0) {
            // delete key since this is an update
            unset($data['cart_item_id']);
            unset($data['unit_price']);

            // prepare data
            foreach ($existingCartItem as $cartItem) {
                $data['weight'] += $cartItem->weight;
                $data['qty'] += $cartItem->qty;
                $data['price'] += $cartItem->price;
            }

            // Update by cart ID
            $this->tableGateway->update($data, ['cart_id' => $cartId, 'product_id' => $productId]);

            // Return cart id
            return $cartId;
        }

        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    public function deleteByCartIdAndProductId($cartId, $productId)
    {
        try {
            return $this->tableGateway->delete(['cart_id' => $cartId, 'product_id' => $productId]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteByCartId($cartId)
    {
        try {
            return $this->tableGateway->delete(['cart_id' => $cartId]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteById($id)
    {
        try {
            return $this->tableGateway->delete(['cart_item_id' => (int)$id]);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
