<?php

namespace Carts\Model;

class Carts
{
    public $cart_id;
    public $customer_id;
    public $order_datetime;
    public $sub_total;
    public $taxable_amount;
    public $discount;
    public $tax;
    public $shipping_total;
    public $total_amount;
    public $total_weight;
    public $company_name;
    public $email;
    public $first_name;
    public $last_name;
    public $phone;
    public $shipping_mehod;
    public $shipping_name;
    public $shipping_address1;
    public $shipping_address2;
    public $shipping_address3;
    public $shipping_city;
    public $shipping_state;
    public $shipping_country;

    public function exchangeArray($data)
    {
        $date = new \DateTime();

        $this->cart_id = (isset($data['cart_id'])) ? $data['cart_id'] : 0;
        $this->customer_id = (isset($data['customer_id'])) ? $data['customer_id'] : '';
        $this->order_datetime = (isset($data['order_datetime']))
            ? $data['order_datetime']
            : $date->format('Y-m-d H:i:s');
        $this->sub_total = (isset($data['sub_total'])) ? $data['sub_total'] : '';
        $this->taxable_amount = (isset($data['taxable_amount'])) ? $data['taxable_amount'] : '';
        $this->discount = (isset($data['discount'])) ? $data['discount'] : '';
        $this->tax = (isset($data['tax'])) ? $data['tax'] : '';
        $this->shipping_total = (isset($data['shipping_total'])) ? $data['shipping_total'] : '';
        $this->total_amount = (isset($data['total_amount'])) ? $data['total_amount'] : '';
        $this->total_weight = (isset($data['total_weight'])) ? $data['total_weight'] : '';
        $this->company_name = (isset($data['company_name'])) ? $data['company_name'] : '';
        $this->email = (isset($data['email'])) ? $data['email'] : '';
        $this->first_name = (isset($data['first_name'])) ? $data['first_name'] : '';
        $this->last_name = (isset($data['last_name'])) ? $data['last_name'] : '';
        $this->phone = (isset($data['phone'])) ? $data['phone'] : '';
        $this->shipping_mehod = (isset($data['shipping_mehod'])) ? $data['shipping_mehod'] : '';
        $this->shipping_name = (isset($data['shipping_name'])) ? $data['shipping_name'] : '';
        $this->shipping_address1 = (isset($data['shipping_address1'])) ? $data['shipping_address1'] : '';
        $this->shipping_address2 = (isset($data['shipping_address2'])) ? $data['shipping_address2'] : '';
        $this->shipping_address3 = (isset($data['shipping_address3'])) ? $data['shipping_address3'] : '';
        $this->shipping_city = (isset($data['shipping_city'])) ? $data['shipping_city'] : '';
        $this->shipping_state = (isset($data['shipping_state'])) ? $data['shipping_state'] : '';
        $this->shipping_country = (isset($data['shipping_country'])) ? $data['shipping_country'] : '';
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
