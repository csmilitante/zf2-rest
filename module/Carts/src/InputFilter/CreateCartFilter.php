<?php

namespace Carts\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\Digits;
use Zend\Validator\NotEmpty;

class CreateCartFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'pid',
            'required' => true,
            'filters' => [
                [
                    'name' => 'Zend\Filter\StringTrim',
                    'options' => [],
                ],
            ],
            'validators' => [
                [
                    'name' => Digits::class,
                ],
                [
                    'name' => NotEmpty::class
                ]
            ],
            'description' => 'Validate product id if integer',
            'allow_empty' => false,
            'continue_if_empty' => false,
        ]);

        $this->add([
            'name' => 'quantity',
            'required' => true,
            'filters' => [
                [
                    'name' => 'Zend\Filter\StringTrim',
                    'options' => [],
                ],
            ],
            'validators' => [
                [
                    'name' => Digits::class,
                ],
                [
                    'name' => NotEmpty::class
                ]
            ],
            'description' => 'Validate quantity if integer',
            'allow_empty' => false,
            'continue_if_empty' => false,
        ]);
    }
}
