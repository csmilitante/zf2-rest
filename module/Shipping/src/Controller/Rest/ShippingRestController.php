<?php

namespace Shipping\Controller\Rest;

use Shipping\InputFilter\ShippingFilter;
use Carts\Model\CartsTable;
use Shipping\Service\ShippingService;
use Zend\View\Model\JsonModel;

class ShippingRestController extends BaseRestController
{
    /**
     * @var CartsTable
     */
    private $cartsTable;
    /**
     * @var ShippingService
     */
    private $shippingService;
    /**
     * @var ShippingFilter
     */
    private $shippingFilter;

    /**
     * ShippingRestController constructor.
     *
     * @param ShippingFilter  $shippingFilter
     * @param                 $config
     * @param                 $server
     * @param ShippingService $shippingService
     * @param CartsTable      $cartsTable
     */
    public function __construct(
        ShippingFilter $shippingFilter,
        $config,
        $server,
        ShippingService $shippingService,
        CartsTable $cartsTable
    ) {
        parent::__construct($config, $server);
        $this->cartsTable = $cartsTable;
        $this->shippingService = $shippingService;
        $this->shippingFilter = $shippingFilter;
    }

    public function get($jwt)
    {
        try {
            $this->checkAuthHeader();

            $response = $this->getResponse();

            // decode JWT
            $decodedJwt = $this->decodeJwtToken($jwt);
            // get total weight of cart item from cart
            $cartsEntity = $this->cartsTable->getById($decodedJwt['carts']['cart_id']);
            $totalCartItemsWeight = $cartsEntity->total_weight;

            $shippingRates = $this->shippingService->getShippingRatesByWeight($totalCartItemsWeight);

            $response->setStatusCode(200);

            return new JsonModel([
                'shipping' => $shippingRates
            ]);
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function create($postData)
    {
        try {
            $this->checkAuthHeader();

            $this->shippingFilter->setData($postData);

            if (!$this->shippingFilter->isValid()) {
                throw new \Exception(json_encode($this->shippingFilter->getMessages()), 406);
            }

            // Decode jwt
            $decodedJwt = $this->decodeJwtToken($postData['jwt']);

            $cartsEntity = $this->cartsTable->getById($decodedJwt['carts']['cart_id']);
            $totalCartItemsWeight = $cartsEntity->total_weight;

            $postData['shipping_total'] = $this->shippingService->getShippingRateByMethodAndWeight(
                $postData['shipping_mehod'],
                $totalCartItemsWeight
            );
            $postData['cart_id'] = $cartsEntity->cart_id;
            $postData['total_weight'] = $totalCartItemsWeight;

            foreach ($postData as $key => $value) {
                $cartsEntity->{$key} = $value;
            }

            $cartsEntity->total_amount = $cartsEntity->sub_total + $cartsEntity->shipping_total;

            // Update cart
            $this->cartsTable->upsert($cartsEntity);

            $response = $this->getResponse();
            $response->setStatusCode(201);

            $encodedJwt = $this->generateJwtToken($decodedJwt);

            return new JsonModel([
                'jwt' => $encodedJwt
            ]);
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }
}
