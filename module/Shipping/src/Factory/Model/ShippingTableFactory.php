<?php

namespace Shipping\Factory\Model;

use Shipping\Model\Shipping;
use Shipping\Model\ShippingTable;
use Interop\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ShippingTableFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dbAdapter = $container->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Shipping());

        $shippingTableGateway = new TableGateway('shipping', $dbAdapter, null, $resultSetPrototype);

        return new ShippingTable($shippingTableGateway);
    }

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $container
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container, ShippingTable::class);
    }
}
