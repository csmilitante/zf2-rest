<?php

namespace Shipping\Factory\Service;

use Shipping\Model\ShippingTable;
use Shipping\Service\ShippingService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ShippingServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $shippingTable = $container->get(ShippingTable::class);

        return new ShippingService($shippingTable);
    }

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $container
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container, ShippingService::class);
    }
}
