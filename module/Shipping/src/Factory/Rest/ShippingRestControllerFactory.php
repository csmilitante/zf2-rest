<?php

namespace Shipping\Factory\Rest;

use Shipping\Controller\Rest\ShippingRestController;
use Shipping\Service\ShippingService;
use Shipping\InputFilter\ShippingFilter;
use Carts\Model\CartsTable;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ShippingRestControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = [])
    {
        $sl = $container->getServiceLocator();
        $config = $sl->get('config');
        $serverFactory = $sl->get('ZF\OAuth2\Service\OAuth2Server');
        $server = call_user_func($serverFactory, 'oauth');
        $shippingFilter = new ShippingFilter();

        $shippingService = $sl->get(ShippingService::class);
        $cartsTable = $sl->get(CartsTable::class);

        return new ShippingRestController(
            $shippingFilter,
            $config,
            $server,
            $shippingService,
            $cartsTable
        );
    }

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $container
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $container)
    {
        return $this($container, ShippingRestController::class);
    }
}
