<?php

namespace Shipping\Service;

use Shipping\Model\ShippingTable;

class ShippingService
{
    /**
     * @var ShippingTable
     */
    private $shippingTable;

    /**
     * ShippingService constructor.
     *
     * @param ShippingTable $shippingTable
     */
    public function __construct(ShippingTable $shippingTable)
    {
        $this->shippingTable = $shippingTable;
    }

    public function getShippingRateByMethodAndWeight($method, $totalCartItemsWeight)
    {
        $rates = $this->shippingTable->getShippingRatesByMethod($method);
        $priceOfExcess = 0;
        $totalRate = 0;

        // Max weight and Shipping rate
        if ($totalCartItemsWeight > $rates[0]['max_weight']) {
            // Excess
            $z = floor($totalCartItemsWeight / $rates[0]['max_weight']);
            $priceOfExcess = $z * $rates[0]['shipping_rate'];

            // compute weight less excess
            $totalCartItemsWeight = $totalCartItemsWeight - ($rates[0]['max_weight'] * $z);
        }

        foreach ($rates as $detail) {
            if ($totalCartItemsWeight >= $detail['min_weight'] &&
                $totalCartItemsWeight <= $detail['max_weight']
            ) {
                $totalRate = $detail['shipping_rate'] + $priceOfExcess;
                break;
            }
        }

        return $totalRate;
    }

    public function getShippingRatesByWeight($totalCartItemsWeight)
    {
        $shippingRates = [];
        // get max weight per method
        $maxWeightPerMethod = $this->shippingTable->getMaxWeightPerMethod();

        $shippingRatesObj = $this->shippingTable->fetchAll();
        $shippingResultset = $shippingRatesObj->toArray();

        // do computations here
        foreach ($maxWeightPerMethod as $index => $method) {
            // Check if $totalCartItemsWeight is over the limit
            if ($totalCartItemsWeight > $method['max_weight']) {
                // do computations for excess of
                $z = floor($totalCartItemsWeight / $method['max_weight']);
                $priceOfExcess = $z * $method['shipping_rate'];

                // get weight less excess and compute
                $weightLessExcess = $totalCartItemsWeight - ($method['max_weight'] * $z);

                $priceLessExcess = 0;

                foreach ($shippingResultset as $key => $detail) {
                    if ($method['shipping_method'] === $detail['shipping_method']) {
                        if ($weightLessExcess >= $detail['min_weight'] &&
                            $weightLessExcess <= $detail['max_weight']
                        ) {
                            $priceLessExcess = $shippingResultset[$key]['shipping_rate'];
                        }
                    }
                }

                $shippingRates[$index]['method'] = $method['shipping_method'];
                $shippingRates[$index]['shipping_rate'] = $priceOfExcess + $priceLessExcess;
            } else {
                foreach ($shippingResultset as $key => $detail) {
                    if ($method['shipping_method'] === $detail['shipping_method']) {
                        if ($totalCartItemsWeight >= $detail['min_weight'] &&
                            $totalCartItemsWeight <= $detail['max_weight']
                        ) {
                            $shippingDetails['shipping_rate'] = $shippingResultset[$key]['shipping_rate'];
                        }
                    }
                }

                $shippingRates[$index]['method'] = $method['shipping_method'];
                $shippingRates[$index]['shipping_rate'] = $shippingDetails['shipping_rate'];
            }
        }
        return $shippingRates;
    }
}
