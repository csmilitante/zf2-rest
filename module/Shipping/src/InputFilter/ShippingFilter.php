<?php

namespace Shipping\InputFilter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;

class ShippingFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(
            [
                'name' => 'shipping_name',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'shipping_address1',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'shipping_city',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'shipping_state',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'shipping_country',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'shipping_mehod',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );

        $this->add(
            [
                'name' => 'jwt',
                'required' => true,
                'filters' => [
                    [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    [
                        'name' => NotEmpty::class
                    ]
                ],
                'allow_empty' => false,
                'continue_if_empty' => false,
            ]
        );
    }
}
