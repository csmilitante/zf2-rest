<?php

namespace Shipping\Model;

use Zend\Db\TableGateway\TableGateway;

class ShippingTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getMaxWeightPerMethod()
    {
        try {
            $query = "SELECT
                   max(max_weight) as max_weight,
                   shipping_method,
                   max(shipping_rate) as shipping_rate
               FROM {$this->tableGateway->getTable()} 
               GROUP BY shipping_method";
            $adapter = $this->tableGateway->getAdapter();

            $stmt = $adapter->createStatement($query);
            $result = $stmt->execute();

            foreach ($result as $val) {
                $rowset[] = $val;
            }

            return $rowset;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getShippingRatesByMethod($shippingMethod)
    {
        try {
            $query = "SELECT
            shipping_rate, min_weight, max_weight from {$this->tableGateway->getTable()}
            WHERE shipping_method = :shippingMethod
            ORDER BY max_weight DESC";
            $adapter = $this->tableGateway->getAdapter();

            $stmt = $adapter->createStatement(
                $query,
                [
                    ':shippingMethod' => $shippingMethod
                ]
            );
            $result = $stmt->execute();

            foreach ($result as $val) {
                $rowset[] = $val;
            }

            return $rowset;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
