<?php

namespace JobOrders\Factory\Model;

use JobOrders\Model\JobOrders;
use JobOrders\Model\JobOrdersTable;
use Interop\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class JobOrdersTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new JobOrders());

        $jobOrdersTableGateway = new TableGateway('job_orders', $dbAdapter, null, $resultSetPrototype);

        return new JobOrdersTable($jobOrdersTableGateway);
    }
}
