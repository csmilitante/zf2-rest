<?php

namespace JobOrders\Factory\Model;

use JobOrders\Model\JobItems;
use JobOrders\Model\JobItemsTable;
use Interop\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class JobItemsTableFactory
{
    public function __invoke(ContainerInterface $container) {
        $dbAdapter = $container->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new JobItems());

        $jobItemsTableGateway = new TableGateway(
            'job_items',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new JobItemsTable($jobItemsTableGateway);
    }
}
