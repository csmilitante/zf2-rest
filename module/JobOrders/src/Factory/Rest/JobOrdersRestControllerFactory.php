<?php

namespace JobOrders\Factory\Rest;

use JobOrders\Controller\Rest\JobOrdersRestController;
use Carts\Model\CartItemsTable;
use Carts\Model\CartsTable;
use JobOrders\Model\JobItemsTable;
use JobOrders\Model\JobOrdersTable;
use Products\Model\ProductsTable;
use Psr\Container\ContainerInterface;

class JobOrdersRestControllerFactory
{
    public function __invoke(ContainerInterface $container) {
        $sl = $container->getServiceLocator();
        $config = $sl->get('config');

        $serverFactory = $sl->get('ZF\OAuth2\Service\OAuth2Server');
        $server = call_user_func($serverFactory, 'oauth');

        $jobOrdersTable = $sl->get(JobOrdersTable::class);
        $jobItemsTable = $sl->get(JobItemsTable::class);
        $productsTable = $sl->get(ProductsTable::class);
        $cartsTable = $sl->get(CartsTable::class);
        $cartItemsTable = $sl->get(CartItemsTable::class);

        return new JobOrdersRestController(
            $config,
            $server,
            $jobOrdersTable,
            $jobItemsTable,
            $productsTable,
            $cartsTable,
            $cartItemsTable
        );
    }
}
