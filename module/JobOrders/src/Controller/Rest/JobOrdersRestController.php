<?php

namespace JobOrders\Controller\Rest;

use Carts\Model\CartItemsTable;
use Carts\Model\CartsTable;
use JobOrders\Model\JobItemsTable;
use JobOrders\Model\JobOrdersTable;
use Products\Model\ProductsTable;
use Zend\View\Model\JsonModel;

class JobOrdersRestController extends BaseRestController
{
    /**
     * @var JobOrdersTable
     */
    private $jobOrdersTable;
    /**
     * @var JobItemsTable
     */
    private $jobItemsTable;
    /**
     * @var ProductsTable
     */
    private $productsTable;
    /**
     * @var CartsTable
     */
    private $cartsTable;
    /**
     * @var CartItemsTable
     */
    private $cartItemsTable;

    public function __construct(
        $config,
        $server,
        JobOrdersTable $jobOrdersTable,
        JobItemsTable $jobItemsTable,
        ProductsTable $productsTable,
        CartsTable $cartsTable,
        CartItemsTable $cartItemsTable
    ) {
        parent::__construct($config, $server);
        $this->jobOrdersTable = $jobOrdersTable;
        $this->jobItemsTable = $jobItemsTable;
        $this->productsTable = $productsTable;
        $this->cartsTable = $cartsTable;
        $this->cartItemsTable = $cartItemsTable;
    }

    public function get($jwt)
    {
        try {
            $this->checkAuthHeader();

            $response = $this->getResponse();

            $decodedJwt = $this->decodeJwtToken($jwt);

            $jobOrder = $this->jobOrdersTable
                ->getById($decodedJwt['job_orders']['job_order_id']);

            $jobItems = $this->jobItemsTable
                ->getByJobOrderId($decodedJwt['job_orders']['job_order_id']);

            // Get product details of each items
            foreach ($jobItems as $index => $item) {
                $product = $this->productsTable->getById($item->product_id);
                $item->product_details = $product;
                $jobItems[$index] = $item;
            }

            $response->setStatusCode(200);

            return new JsonModel(
                [
                    'job_orders' => $jobOrder,
                    'job_items' => $jobItems
                ]
            );
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function create($data)
    {
        try {
            $this->checkAuthHeader();

            $response = $this->getResponse();

            $decodedJwt = $this->decodeJwtToken($data['jwt']);
            $cartId = $decodedJwt['carts']['cart_id'];

            // Get Cart to be transferred to JobOrders
            $cartObj = $this->cartsTable->getById($cartId);

            $jobOrderId = $this->jobOrdersTable
                ->transferCartToJobOrdersByCartId($cartObj);

            // Get all cart items from cart
            $cartItemsArr = $this->cartItemsTable->getByCartId($cartObj->cart_id);

            $this->jobItemsTable
                ->transferCartItemsToJobItems($cartItemsArr, $jobOrderId);

            // Update product quantity
            foreach ($cartItemsArr as $item) {
                $product = $this->productsTable->getById($item->product_id);
                $product->stock_qty = $product->stock_qty - $item->qty;

                $this->productsTable->upsert($product);
            }

            // Add job_order_id to return
            $decodedJwt['job_orders']['job_order_id'] = $jobOrderId;

            // Re-encode JWT
            $jwt = $this->generateJwtToken($decodedJwt);

            $response->setStatusCode(201);

            return new JsonModel(
                [
                    'jwt' => $jwt
                ]
            );
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function deleteList($data)
    {
        try {
            $this->checkAuthHeader();

            $response = $this->getResponse();

            // Decode jwt
            $decodedJwt = $this->decodeJwtToken($data['jwt']);

            // delete all cart items by cart Id
            $this->cartItemsTable->deleteByCartId($decodedJwt['carts']['cart_id']);

            // delete cart
            $this->cartsTable->deleteById($decodedJwt['carts']['cart_id']);

            $response->setStatusCode(200);

            $encodedReturn = $this->generateJwtToken(
                [
                    'success' => true
                ]
            );

            return new JsonModel(
                [
                    'jwt' => $encodedReturn
                ]
            );
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }
}
