<?php

namespace JobOrders\Controller\Rest;

interface RestControllerInterface
{
    public function generateJwtToken($payload);
    public function decodeJwtToken($jwt);
}
