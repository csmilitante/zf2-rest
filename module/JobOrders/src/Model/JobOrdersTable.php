<?php

namespace JobOrders\Model;

use Carts\Model\Carts;
use Zend\Db\TableGateway\TableGateway;

class JobOrdersTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getById($jobOrderId)
    {
        $id = (int) $jobOrderId;
        $rowset = $this->tableGateway->select(['job_order_id' => $id]);
        $row = $rowset->current();

        if (!$row) {
            throw new \Exception('Could not find Job Order', 500);
        }

        return $row;
    }

    public function transferCartToJobOrdersByCartId(Carts $cart)
    {
        try {
            $cartArr = $cart->toArray();

            $jobOrdersModel = new JobOrders();
            $jobOrdersModel->exchangeArray($cartArr);

            // Update order_datetime
            $date = new \DateTime();
            $jobOrdersModel->order_datetime = $date->format('Y-m-d H:i:s');

            $this->tableGateway->insert($jobOrdersModel->toArray());

            return $this->tableGateway->getLastInsertValue();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
