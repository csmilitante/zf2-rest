<?php

namespace JobOrders\Model;

class JobItems
{
    public $job_item_id;
    public $job_order_id;
    public $product_id;
    public $weight;
    public $qty;
    public $unit_price;
    public $price;

    public function exchangeArray($data)
    {
        $this->job_item_id = (isset($data['job_item_id'])) ? $data['job_item_id'] : 0;
        $this->job_order_id = (isset($data['job_order_id'])) ? $data['job_order_id'] : '';
        $this->product_id = (isset($data['product_id'])) ? $data['product_id'] : '';
        ;
        $this->weight = (isset($data['weight'])) ? $data['weight'] : '';
        ;
        $this->qty = (isset($data['qty'])) ? $data['qty'] : '';
        ;
        $this->unit_price = (isset($data['unit_price'])) ? $data['unit_price'] : '';
        ;
        $this->price = (isset($data['price'])) ? $data['price'] : '';
        ;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}
