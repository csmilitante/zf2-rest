<?php

namespace JobOrders\Model;

use Zend\Db\TableGateway\TableGateway;

class JobItemsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getByJobOrderId($jobOrderId)
    {
        try {
            $id = (int)$jobOrderId;
            $rowset = $this->tableGateway->select(['job_order_id' => $id]);

            if ($rowset->count() <= 0) {
                throw new \Exception('Could not find record job item', 500);
            }

            foreach ($rowset as $jobItem) {
                $row[] = $jobItem;
            }

            return $row;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function transferCartItemsToJobItems($cartItems, $jobOrderId)
    {
        try {
            $jobItemsModel = new JobItems();

            // Loop through cart items and insert
            foreach ($cartItems as $key => $item) {
                $jobItemsModel->exchangeArray($item->toArray());
                $jobItemsModel->job_order_id = $jobOrderId;

                $this->tableGateway->insert($jobItemsModel->toArray());
            }
        } catch (\Exception $e) {
            throw $e;
        }
        /**/
    }
}
