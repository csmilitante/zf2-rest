<?php

namespace Products\Model;

use Zend\Db\TableGateway\TableGateway;

class ProductsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getById($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['product_id' => $id]);

        $row = $rowset->current();

        if (!$row) {
            throw new \Exception('Product does not exist.', 500);
        }

        return $row;
    }

    public function upsert(Product $product)
    {
        $data = [
            'product_name' => $product->product_name,
            'product_desc' => $product->product_desc,
            'product_image' => $product->product_image,
            'product_thumbnail' => $product->product_thumbnail,
            'weight' => $product->weight,
            'price' => $product->price,
            'stock_qty'=> $product->stock_qty,
            'taxable_flag' => $product->taxable_flag
        ];

        $id = (int) $product->product_id;

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getById($id)) {
                return $this->tableGateway->update($data, ['product_id' => $id]);
            } else {
                throw new \Exception('Record does not exist', 500);
            }
        }
    }

    public function deleteById($id)
    {
        try {
            $this->tableGateway->delete(['product_id' => (int) $id]);
        } catch (\Exception $e) {
        }
    }
}
