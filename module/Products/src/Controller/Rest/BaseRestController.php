<?php

namespace Products\Controller\Rest;

use Application\CustomExceptions\AuthException;
use Application\CustomExceptions\FormFilterException;
use Application\CustomExceptions\JwtException;
use Firebase\JWT\JWT;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class BaseRestController extends AbstractRestfulController implements RestControllerInterface
{
    /**
     * @var
     */
    private $config;
    /**
     * @var
     */
    private $server;

    public function __construct($config, $server)
    {
        $this->config = $config;
        $this->server = $server;
    }

    protected function getOptions()
    {
        if ($this->params()->fromRoute('id', false)) {
            // There is an ID
            return $this->config['api']['options']['resource'];
        }

        // No ID
        return $this->config['api']['options']['collection'];
    }

    public function options()
    {
        $response = $this->getResponse();

        // If in options array, allow
        $response->getHeaders()
                 ->addHeaderLine('Allow', implode(',', $this->getOptions()));

        return $response;
    }

    public function setEventManager(EventManagerInterface $events)
    {
        parent::setEventManager($events);

        // Register the listener and callback method with a priority of 10
        $events->attach('dispatch', [$this, 'checkOptions'], 10);
    }

    public function checkOptions($e)
    {
        if (!in_array($e->getRequest()->getMethod(), $this->getOptions())) {
            // Method not allowed
            $response = $this->getResponse();
            $response->setStatusCode(405);

            return $response;
        }

        return;
    }

    public function checkAuthHeader()
    {
        $authHeader = $this->getAuthHeader();

        if ($authHeader === false) {
            $this->getResponse()->setStatusCode(401);
            throw new \Exception('Authorization required', 401);
        }

        $accessToken = $this->getAccessToken();

        try {
            // verify access token
            return $this->decodeJwtToken($accessToken);
        } catch (\Exception $e) {
            throw new AuthException('You have been logged out. Please log in again.', 401);
        }
    }

    public function getAuthHeader()
    {
        $request = $this->getRequest();
        $headers = $request->getHeaders();

        return $headers->get('Authorization');
    }

    public function getAccessToken()
    {
        $authHeader = $this->getAuthHeader();

        return preg_replace("/Bearer /", "", $authHeader->getFieldValue());
    }

    public function exceptionHandler(\Exception $e)
    {
        $response = $this->getResponse();

        if ($e->getCode() === 0) {
            $response->setStatusCode(500);
        } else {
            $response->setStatusCode($e->getCode());
        }

        $title = 'Whooooops :(';
        $message = $e->getMessage();

        if ($e instanceof AuthException) {
            $title = 'Authentication error:';
            $type = 'auth_expired';
        } elseif ($e instanceof FormFilterException) {
            $title = 'Invalid form error:';
            $type = 'form_filter';
        } else {
            $type = 'default';
        }

        return new JsonModel(
            [
                'type' => $type,
                'title' => $title,
                'status_code' => $e->getCode(),
                'message' => $message
            ]
        );
    }

    public function generateJwtToken($payload)
    {
        $jwtConfig = $this->config['jwt'];
        $key = base64_decode($jwtConfig['key']);

        $tokenId = uniqid();
        $issuedAt = time();
        $notBefore = $issuedAt + $jwtConfig['nbf_offset'];
        $expire = $notBefore + $jwtConfig['exp_offset'];

        $jwt = [
            'iat' => $issuedAt,
            'jti' => $tokenId,
            'nbf' => $notBefore,
            'exp' => $expire,
            'iss' => $jwtConfig['iss'], // should be in the config
            'payload' => $payload
        ];

        return JWT::encode($jwt, $key);
    }

    public function decodeJwtToken($jwt)
    {
        $jwtConfig = $this->config['jwt'];
        $key = base64_decode($jwtConfig['key']);

        try {
            $decoded = JWT::decode($jwt, $key, [$jwtConfig['alg']]);

            // @todo Do additional checks here, like iss?

            return json_decode(json_encode($decoded->payload), true);
        } catch (\Exception $e) {
            throw new JwtException('Token is invalid. Please try again.', 500);
        }
    }
}
