<?php

namespace Products\Controller\Rest;

use Products\Model\Product;
use Products\Model\ProductsTable;
use Zend\View\Model\JsonModel;
use OAuth2\Request as OAuth2Request;

class ProductsRestController extends BaseRestController
{
    /** @var ProductsTable */
    protected $productsTable;
    /**
     * @var \OAuth2\Server
     */
    private $server;

    public function __construct(ProductsTable $productsModel, $server, $config)
    {
        parent::__construct($config, $server);
        $this->productsTable = $productsModel;
    }

    public function create($data)
    {
        $response = $this->getResponse();

        $productEntity = new Product();
        $productEntity->exchangeArray($data);

        try {
            $result = $this->productsTable->upsert($productEntity);
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }

        $response->setStatusCode(201);

        // send back data
        return new JsonModel(
            [
                'success' => $result
            ]
        );
    }

    public function get($id)
    {
        $response = $this->getResponse();

        try {
            $product = $this->productsTable->getById($id);
            $response->setStatusCode(200);

            return new JsonModel(
                [
                    'details' => $product,
                    '_links' => [
                        'to_cart' => '/api/carts'
                    ]
                ]
            );
        } catch (\Exception $e) {
            return $this->exceptionHandler($e);
        }
    }

    public function getList()
    {
        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->getHeaders()->addHeaderLine('Content-type', 'application/hal+json');

        $productList = $this->productsTable->fetchAll()->toArray();

        /*foreach ($this->productsTable->fetchAll() as $row) {
            $productList[] = $row;
        }*/

        return new JsonModel(
            [
                'products' => $productList,
                '_links' => [
                    'view' => '/api/products/[product_id]'
                ]
            ]
        );
    }

    public function update($id, $data)
    {
        $response = $this->getResponse();
        $response->setStatusCode(200);

        return new JsonModel(
            [
                'success' => 'woot'
            ]
        );
    }

    public function delete($id)
    {
        // TODO Do some validations here
        $this->productsTable->deleteById($id);

        $response = $this->getResponse();
        $response->setStatusCode(200);

        return new JsonModel(
            [
                'success' => true,
                'message' => 'Record has been deleted'
            ]
        );
    }

    public function deleteList($data)
    {
        $response = $this->getResponse();
        $response->setStatusCode(400);

        return new JsonModel(
            [
                'error' => [
                    'http_status_code' => '400',
                    'message' => 'ID is required'
                ]
            ]
        );
    }

    /*private function hasValidAccessToken()
    {
        if (!$this->server->verifyResourceRequest(OAuth2Request::createFromGlobals())) {
            // Not authorized return 401 error
            $this->getResponse()->setStatusCode(401);

            return false;
        }

        return true;
    }

    private function return401Response() {
        return new JsonModel(
            [
                'error' => 'UNAUTHORIZED',
                'http_code' => '401',
                'message' => 'Access token is not valid.',
                'ref' => 'https://httpstatuses.com/401'
            ]
        );
    }*/
}
