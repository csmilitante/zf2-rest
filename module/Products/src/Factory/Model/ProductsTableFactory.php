<?php

namespace Products\Factory\Model;

use Products\Model\Product;
use Products\Model\ProductsTable;
use Interop\Container\ContainerInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class ProductsTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('Zend\Db\Adapter\Adapter');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Product());

        $productsTableGateway = new TableGateway('products', $dbAdapter, null, $resultSetPrototype);

        return new ProductsTable($productsTableGateway);
    }
}
