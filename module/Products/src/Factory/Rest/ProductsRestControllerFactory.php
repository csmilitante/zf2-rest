<?php

namespace Products\Factory\Rest;

use Products\Controller\Rest\ProductsRestController;
use Products\Model\ProductsTable;
use Interop\Container\ContainerInterface;

class ProductsRestControllerFactory
{

    public function __invoke(ContainerInterface $container)
    {
        $sl = $container->getServiceLocator();
        $productsModel = $sl->get(ProductsTable::class);
        $config = $sl->get('config');

        $serverFactory = $sl->get('ZF\OAuth2\Service\OAuth2Server');
        $server = call_user_func($serverFactory, 'oauth');

        return new ProductsRestController($productsModel, $server, $config);
    }
}
